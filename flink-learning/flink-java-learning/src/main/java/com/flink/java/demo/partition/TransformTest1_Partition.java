package com.flink.java.demo.partition;

import com.flink.java.demo.FlinkConstant;
import com.flink.java.demo.bean.SensorReading;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * 重新分区 算子
 */
public class TransformTest1_Partition {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());

        env.setParallelism(4);

        // 从文件读取数据
        DataStream<String> inputStream = env.readTextFile(FlinkConstant.sensorPath);

        // 转换成SensorReading类型
        DataStream<SensorReading> dataStream = inputStream.map(line -> {
            String[] fields = line.split(",");
            return new SensorReading(fields[0], new Long(fields[1]), new Double(fields[2]));
        });
//        dataStream.print("input");

        // 1. huffle随机分区: random.nextInt(下游算子并行度)
        DataStream<String> shuffleStream = inputStream.shuffle();
//        shuffleStream.print("shuffle");

        // 2.  keyby: 按指定key去发送，相同key发往同一个子任务  one-to-one: Forward分区器
//        dataStream.keyBy("id").print("keyBy");

        // 3. global: 所有数据全部发送到下游的第一个分区
        dataStream.global().print("global");

        // 4. rebalance轮询：nextChannelToSendTo = (nextChannelToSendTo + 1) % 下游算子并行度
        // 如果是 数据源倾斜的场景， source后，调用rebalance，就可以解决 数据源的 数据倾斜
//        socketDS.rebalance().print();

        //5. rescale缩放： 实现轮询， 局部组队，比rebalance更高效
//        socketDS.rescale().print();


        // 6.broadcast 广播：  发送给下游所有的子任务
//        socketDS.broadcast().print();


        // 总结： Flink提供了 7种分区器+ 1种自定义

        env.execute();

        Thread.sleep(20000000);
    }
}
