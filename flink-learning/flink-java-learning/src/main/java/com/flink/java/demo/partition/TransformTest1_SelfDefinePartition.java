package com.flink.java.demo.partition;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * 自定义分区 算子
 */
public class TransformTest1_SelfDefinePartition {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.createLocalEnvironmentWithWebUI(new Configuration());

        env.setParallelism(3);

        // 从文件读取数据
        DataStream<String> dataStream = env.socketTextStream("192.168.137.101", 7777);

        /**
         * 第一个参数里的第一个参数key：  第二个参数keySelector返回的key
         *
         */
        dataStream.partitionCustom((key, partitionNumber)->{
            return Integer.valueOf(key) % partitionNumber;
        }, key -> key)
        .print();

        env.execute();
    }
}
