package com.flink.java.demo.source;
import com.flink.java.demo.FlinkConstant;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;


public class SourceTest2_File_Old {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        // 从文件读取数据
        DataStream<String> dataStream = env.readTextFile(FlinkConstant.sensorPath);

        // 打印输出
        dataStream.print();

        env.execute();
    }
}
