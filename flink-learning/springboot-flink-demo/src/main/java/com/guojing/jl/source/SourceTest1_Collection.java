package com.guojing.jl.source;
import com.guojing.jl.bean.EventData;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Arrays;

/**
 * 从集合读取数据
 */
public class SourceTest1_Collection {

    public static void main(String[] args) throws Exception{
        // 创建执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        // 从集合中读取数据
        DataStream<EventData> dataStream = env.fromCollection(Arrays.asList(
                new EventData("sensor_1", 1547718199L, 35.8),
                new EventData("sensor_6", 1547718201L, 15.4),
                new EventData("sensor_7", 1547718202L, 6.7),
                new EventData("sensor_10", 1547718205L, 38.1)
        ));

        //从元素读取数据
//        DataStream<Integer> integerDataStream = env.fromElements(1, 2, 4, 67, 189);
        DataStream<EventData> integerDataStream = env.fromElements(
                new EventData("sensor_1", 1547718199L, 35.8),
                new EventData("sensor_6", 1547718201L, 15.4),
                new EventData("sensor_7", 1547718202L, 6.7),
                new EventData("sensor_10", 1547718205L, 38.1));

        // 打印输出
        dataStream.print("data");
        integerDataStream.print("int");

        // 执行
        env.execute();
    }
}
