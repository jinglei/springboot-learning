package com.guojing.jl.netty.bytebuf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

public class ByteBufDemo {

    public static void main(String[] args) {
        // 创建一个ByteBuf，默认容量为16字节
        ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer();
        // 写入数据 (写入的数据类型必须与ByteBuf的类型一致)
        byteBuf.writeBoolean(true);
        byteBuf.writeByte((byte) 127);
        byteBuf.writeShort((short) 32767);
        byteBuf.writeInt(2147483647);

    }

}
