package com.guojing.jl.netty.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public class ClientDemo1 {


    public static void main(String[] args) {

        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(new NioEventLoopGroup())
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
                        // TODO Auto-generated method stub
                    }
                });
        ChannelFuture channelFuture = bootstrap.connect("localhost", 8080);
        try {
            channelFuture.sync()
                    .channel ();

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Hello World!");

    }
}
