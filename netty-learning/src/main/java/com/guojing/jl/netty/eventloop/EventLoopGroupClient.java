package com.guojing.jl.netty.eventloop;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;

import java.net.InetSocketAddress;

public class EventLoopGroupClient {
    public static void main(String[] args) throws InterruptedException {
        // 1. 启动类
        Bootstrap bootstrap = new Bootstrap()
                // 2. 添加 EventLoop
                .group(new NioEventLoopGroup())
                // 3. 选择客户端 channel 实现
                .channel(NioSocketChannel.class)
                // 4. 添加处理器
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override // 在连接建立后被调用
                    protected void initChannel(NioSocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new StringEncoder());
                    }
                });
        // 5. 连接到服务器
        ChannelFuture channelFuture = bootstrap.connect(new InetSocketAddress("localhost", 8080));
        Channel channel = channelFuture.sync()
                .channel();

        // 6. 向服务器发送数据
        channel.writeAndFlush("hello, world");
        System.out.println(channel);
    }
}
