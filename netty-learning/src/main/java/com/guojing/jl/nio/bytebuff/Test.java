package com.guojing.jl.nio.bytebuff;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Test {
    public static void main(String[] args) {

        BigDecimal total = new BigDecimal("1900000");

        BigDecimal rate = new BigDecimal("0.0375");

        //剩余本金
        BigDecimal pricipalRemain = total;
        for (int i = 1; i <= 26; i++) {

            for (int j = 1; j <= 12; j++) {

                BigDecimal multiply = pricipalRemain.multiply(rate);
                BigDecimal currReturnTotal = multiply.add(pricipalRemain);

                BigDecimal divide = currReturnTotal.divide(new BigDecimal("26"), 2, RoundingMode.HALF_UP);
                System.out.println(divide.toPlainString());

            }
        }




    }

}
