package com.guojing.jl.nio.bytebuff;

import com.guojing.jl.nio.util.ByteBufferUtil;

import java.nio.ByteBuffer;

public class Test3ByteBufferRead {
    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        buffer.put(new byte[]{'a', 'b', 'c'});
        buffer.flip();


        ByteBufferUtil.debugAll(buffer);

        //从头开始读: 把a,b,c,d全部读取完毕
        buffer.get(new byte[3]);

        ByteBufferUtil.debugAll(buffer);

        //rewind从头开始读
        buffer.rewind();
        ByteBufferUtil.debugAll(buffer);
        byte b = buffer.get();
        System.out.println((char)b);

        //mark & reset
        //mark 是在读取时，做一个标记记录position位置，即使 position 改变，只要调用 reset 就能回到 mark 的位置


        //get(i)读取，不会改变positon位置,  get()会改变
        System.out.println((char)buffer.get(2));
        System.out.println((char)buffer.get());


    }
}
