package com.guojing.jl.nio.bytebuff;

import com.guojing.jl.nio.util.ByteBufferUtil;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * 分散读取文件
 */
public class Test6ScatteringReads {
    public static void main(String[] args) {
        try  {
            FileChannel channel = new RandomAccessFile("D:\\jl_project\\src\\springboot-learning\\netty-learning\\words.txt", "r").getChannel();


            ByteBuffer b1 = ByteBuffer.allocate(3);
            ByteBuffer b2 = ByteBuffer.allocate(3);
            ByteBuffer b3 = ByteBuffer.allocate(8);

            //把文件一次性读取到三个ByteBuffer
            channel.read(new ByteBuffer[]{b1, b2, b3});

            b1.flip();
            b2.flip();
            b3.flip();

            ByteBufferUtil.debugAll(b1);
            ByteBufferUtil.debugAll(b2);
            ByteBufferUtil.debugAll(b3);
        } catch (IOException e) {
        }
    }
}
