package com.guojing.jl.nio.mutithread;

import lombok.SneakyThrows;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import static com.guojing.jl.nio.util.ByteBufferUtil.debugAll;

public class Worker implements Runnable {

    private Selector selector;
    private String name;
    private volatile boolean start = false; // 还未初始化
    private Thread thread;

    private ConcurrentLinkedQueue<Runnable> queue = new ConcurrentLinkedQueue<>();

    public Worker(String name) {
        this.name = name;
    }

    public void register(SocketChannel clientChannel) throws IOException {
        if (!start) {
            selector = Selector.open();
            thread = new Thread(this, name);
            thread.start();
            start = true;
        }
        queue.add(() -> {
            try {
                clientChannel.register(selector, SelectionKey.OP_READ);
            } catch (ClosedChannelException e) {
                throw new RuntimeException(e);
            }
        });
        // 唤醒selector
        selector.wakeup();

    }


    @SneakyThrows
    public void run() {
        while (true) {
            //和clientChannel.register(selector, SelectionKey.OP_READ);如果不在一个线程会被suspend住
            selector.select();
            Runnable task = queue.poll();
            if(task != null){
                task.run();//执行注册
            }

            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()) {
                SelectionKey selectionKey = iterator.next();
                iterator.remove();
                if (selectionKey.isReadable()) {
                    SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(1024);
                    socketChannel.read(buffer);
                    buffer.flip();
                    debugAll(buffer);
                }
            }
        }

    }

}