package com.guojing.jl.nio.selector;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class ReadClient {

    public static void main(String[] args) throws IOException {

        SocketChannel socketChannel = SocketChannel.open();
//        socketChannel.configureBlocking(false);

        socketChannel.connect(new InetSocketAddress("localhost", 9098));

        //接受数据
        int length = 0;
        while (true){
            ByteBuffer buffer = ByteBuffer.allocate(1024 * 1024);
            //read返回实际读取到的字节数
            length = length + socketChannel.read(buffer);
            System.out.println(length);
        }


    }

}
