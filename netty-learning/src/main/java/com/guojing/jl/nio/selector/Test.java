package com.guojing.jl.nio.selector;

import java.nio.ByteBuffer;

public class Test {


    public static void main(String[] args) {

        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put("1".getBytes());
        buffer.put("2".getBytes());
        buffer.put("345678".getBytes());
        System.out.println("position="+buffer.position() + ", limit="+ buffer.limit());

        buffer.flip();
        System.out.println("position="+buffer.position() + ", limit="+ buffer.limit());

        buffer.compact();
        System.out.println("position="+buffer.position() + ", limit="+ buffer.limit());
    }

}
