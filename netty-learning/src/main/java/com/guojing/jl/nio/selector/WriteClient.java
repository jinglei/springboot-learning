package com.guojing.jl.nio.selector;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

public class WriteClient {

    public static void main(String[] args) throws IOException {

        SocketChannel socketChannel = SocketChannel.open();
//        socketChannel.configureBlocking(false);

        socketChannel.connect(new InetSocketAddress("localhost", 8089));

        socketChannel.write(Charset.defaultCharset().encode("行"));

        System.out.println();



    }

}
