package com.guojing.jl.nio.selector;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

public class WriterServer2 {


    public static void main(String[] args) throws IOException {


        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);

        Selector selector = Selector.open();
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        serverSocketChannel.bind(new InetSocketAddress(9098));

        while (true){
            selector.select();
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()){
                SelectionKey selectionKey = iterator.next();
                iterator.remove();
                if(selectionKey.isAcceptable()){
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    socketChannel.configureBlocking(false);
                    SelectionKey socketSelKey = socketChannel.register(selector, SelectionKey.OP_READ);

                    // 1. 向客户端发送大量数据
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < 30000000; i++) {
                        sb.append("a");
                    }
                    ByteBuffer buffer = Charset.defaultCharset().encode(sb.toString());
                    //实际写入的长度: 写入大字段的时候，会分多次写入
                    int write = socketChannel.write(buffer);
                    System.out.println(write);

                    //是否有未写入的
                    if (buffer.hasRemaining()){
                        //关注可写事件：由于写大量数据可能造成阻塞式，所以关注下可写事件的，当所有数据都写完的时候会变成可写，并接收到可写事件
//                        selectionKey.interestOps(selectionKey.interestOps() + SelectionKey.OP_WRITE);
                        socketSelKey.interestOps(socketSelKey.interestOps() | SelectionKey.OP_WRITE);
                        //未写完的数据挂载selectionKey上
                        System.out.println("第一次写：" + write);
                        socketSelKey.attach(buffer);
                    }

                }else if(selectionKey.isWritable()){

                    ByteBuffer buffer = (ByteBuffer) selectionKey.attachment();
                    SocketChannel channel = (SocketChannel) selectionKey.channel();
                    int write = channel.write(buffer);
                    System.out.println("还有可写数据：" + write);

                    //6.清理
                    if(!buffer.hasRemaining()){
                        selectionKey.attach(null);
                        //内容都写完了不再关注可写就行了
                        selectionKey.interestOps(selectionKey.interestOps() - SelectionKey.OP_WRITE);
                    }
                }

            }

        }

    }

}
