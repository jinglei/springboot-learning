package com.guojing.jl.nio.serverscocket;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

@Slf4j
public class Client {

    public static void main(String[] args) throws IOException {

        SocketChannel channel = SocketChannel.open();

        channel.connect(new InetSocketAddress("localhost", 8080));
        System.in.read();

        channel.write(Charset.defaultCharset().encode("123456781234567812345678\nab"));

        System.out.println();
        System.in.read();

    }
}
