package com.jl.rocketmq.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class MQSpringBootApplication {


    public static void main(String[] args) {
        SpringApplication.run(MQSpringBootApplication.class);
        log.info("消费者启动成功");
    }


}
