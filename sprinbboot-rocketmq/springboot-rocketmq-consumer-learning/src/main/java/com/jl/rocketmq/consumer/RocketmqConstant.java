package com.jl.rocketmq.consumer;

/**
 * @description: TODO
 * @author: jl
 * @create: 2021/4/5 12:08
 */
public class RocketmqConstant {

    public static final String mq_namesrv_ip = "192.168.138.136:9876";

    /**
     * 请根据自己的ip修改
     */
    public static final String mq_namesrv_cluster = "192.168.137.102:9876;192.168.137.103:9876";

    public static final String mq_namesrv_cluster_localhost = "localhost:9876;localhost:8876";

    public static final String producerGroup1 = "producerGroupTest123456";
    public static final String consumerGroup1 = "consumerGroupTest1";

    public static final String transProducerGroup = "transProducerGroup";
    public static final String transconsumerGroup1 = "transconsumerGroup1";

    public static final String topic1 = "topicTest_jl";
    public static final String topic2 = "topicTest02";
    /**
     * 有序的消息topic
      */
    public static final String topic_order = "OrderTopic";
    public static final String topic_order2 = "OrderTopic2";
    public static final String topic_order3 = "OrderTopic3";
    /**
     * 延迟的消息topic
     */
    public static final String topic_delay = "DelayTopic";
    /**
     * 根据标签过滤消息
     */
    public static final String topic_filtertag = "FilterTagTopic";
    public static final String topic_filtersql = "FilterSQLTopic";


    public static final String topic_trans = "TransactionTopic";

    public static final String tag_sync = "tag_sync";
    public static final String tag_async = "tag_async";
    public static final String tag_oneway = "tag_oneway";
    public static final String tag_order = "Order";
    public static final String tag_delay = "Delay";
    public static final String tag_filtertag1 = "FilterTag1";
    public static final String tag_filtertag2 = "FilterTag2";

}
