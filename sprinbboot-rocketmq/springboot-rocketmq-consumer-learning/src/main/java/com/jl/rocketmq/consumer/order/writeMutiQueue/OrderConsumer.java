package com.jl.rocketmq.consumer.order.writeMutiQueue;

import com.jl.rocketmq.consumer.RocketmqConstant;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 当多个消费者往同一个topic的不同队列发送消息的时候，消费者会启动多个线程消费消息
 * 除非设置
 *  consumer.setConsumeThreadMax(1);
 *  consumer.setConsumeThreadMin(1);
 *  但是这也不能保证多个queue之间是有序的，只能保证每个queue是有序的
 *
 */
public class OrderConsumer {
    public static void main(String[] args) throws MQClientException {
        //1.创建消费者Consumer，制定消费者组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(RocketmqConstant.producerGroup1);
        //2.指定Nameserver地址
        consumer.setNamesrvAddr(RocketmqConstant.mq_namesrv_cluster);
        //3.订阅主题Topic和Tag
        consumer.subscribe(RocketmqConstant.topic_order3, "*");

        AtomicInteger count = new AtomicInteger();

        consumer.setConsumeThreadMax(1);
        consumer.setConsumeThreadMin(1);

        //4.注册消息监听器
        consumer.registerMessageListener(new MessageListenerOrderly() {

            @Override
            public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs, ConsumeOrderlyContext context) {
                for (MessageExt msg : msgs) {
                    int countI = count.incrementAndGet();
                    System.out.println("消息数量"+countI+",线程名称：【" + Thread.currentThread().getName() + "】:" + new String(msg.getBody()) +"  "+msg.toString());
                }
                return ConsumeOrderlyStatus.SUCCESS;
            }
        });

        //5.启动消费者
        consumer.start();

        System.out.println("消费者启动");

    }
}
