package com.jl.rocketmq.producer.demo;


import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

@Slf4j
public class ProducerTest {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    public void test1(){
        rocketMQTemplate.convertAndSend("springboot-rocketmq-learning","hello springboot rocketmq");
        log.info("消息发送成功：" + new Date().toLocaleString());
    }
}
