package com.jl.rocketmq.producer.demo.controller;

import com.jl.rocketmq.producer.demo.dto.Result;
import com.jl.rocketmq.producer.demo.dto.User;
import com.jl.rocketmq.producer.demo.service.MQProducerService;
import org.apache.rocketmq.client.producer.SendResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rocketmq")
public class RocketMQController {

    @Autowired
    private MQProducerService mqProducerService;

    @GetMapping("/send")
    public void send(String topic) {
        User user = new User("test");
        mqProducerService.send(topic, user);
    }

    @GetMapping("/sendTag")
    public Result<SendResult> sendTag(String topic) {
        SendResult sendResult = mqProducerService.sendTagMsg(topic,"带有tag的字符消息");
        return Result.success(sendResult);
    }

}
