package com.jl.rocketmq.producer.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private int age;
    private String name;

    public User(String name){
        this.name = name;
    }

}
