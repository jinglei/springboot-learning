##如何保证消息的有序性

### 1. 生产者方面
- 往同一个topic的同一个queue里发送消息
```java
SendResult sendResult = producer.send(message, new MessageQueueSelector() {
                /**
                 *
                 * @param mqs：队列集合
                 * @param msg：消息对象
                 * @param arg：业务标识的参数
                 * @return
                 */
                @Override
                public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
                    long orderId = (long) arg;
                    long index = orderId % mqs.size();
                    return mqs.get((int) index);
                }
            }, orderSteps.get(i).getOrderId());
```             

- 只能一个线程发送消息

### 2.消费者方面

    MessageListenerOrderly： 顺序消费，对一个queue开启一个线程，多个queue开启多个线程
    MessageListenerConcurrently 会并发消费，开启多个线程。同一个group里的多个consumer并发消费