package com.jl.rocketmq.producer.demo.producer;

import com.jl.rocketmq.consumer.RocketmqConstant;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;

/**
 * @description: 单向消息 生产者
 * @author: jl
 * @create: 2021/4/5 11:51
 */
public class OneWayProducer {

    public static void main(String[] args) throws Exception {
        DefaultMQProducer producer = new DefaultMQProducer(RocketmqConstant.producerGroup1);
        producer.setSendMsgTimeout(10000);

        //配置mqnamesrv地址
        producer.setNamesrvAddr(RocketmqConstant.mq_namesrv_cluster);
        producer.start();

        for (int i = 0; i < 1; i++) {
            Message msg = new Message(RocketmqConstant.topic1,RocketmqConstant.tag_oneway, ("第"+i+"条单向测试消息").getBytes());
//       单项消息只发送消息，不等待服务器响应，也就没有返回值了。此方式发送消息的过程耗时非常短，一般在微秒级别。
            producer.sendOneway(msg);
        }
        producer.shutdown();
        System.out.println("消息发送完毕");
    }

    /**
     * @description: 生产者：发送消息带有标签
     * @author: jl
     * @create: 2021/4/5 11:51
     */
    public static class Producer4 {

        public static void main(String[] args) throws Exception {
            DefaultMQProducer producer = new DefaultMQProducer("jlgroup");
            producer.setSendMsgTimeout(10000);

            //配置mqnamesrv地址
            producer.setNamesrvAddr(RocketmqConstant.mq_namesrv_cluster);
            producer.start();

            Message msg = new Message(RocketmqConstant.topic2, "tag-1","jlKey","第22条测试消息tag-1".getBytes());

            producer.send(msg);

            producer.shutdown();
            System.out.println("消息发送完毕");

        }

    }
}
