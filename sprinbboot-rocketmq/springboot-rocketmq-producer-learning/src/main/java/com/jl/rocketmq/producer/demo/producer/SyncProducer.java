package com.jl.rocketmq.producer.demo.producer;

import com.jl.rocketmq.consumer.RocketmqConstant;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

/**
 * @description: 同步发送消息 生产者
 * @author: jl
 * @create: 2021/4/5 11:51
 */
public class SyncProducer {

    public static void main(String[] args) throws Exception {
        DefaultMQProducer producer = new DefaultMQProducer(RocketmqConstant.producerGroup1);
        producer.setSendMsgTimeout(1000000);

        //配置mqnamesrv地址
        producer.setNamesrvAddr(RocketmqConstant.mq_namesrv_cluster_localhost);
        //启动生产者
        producer.start();

        for (int i = 0; i < 1; i++) {
            //消息
            Message msg = new Message(RocketmqConstant.topic1,RocketmqConstant.tag_sync, ("第"+i+"条同步测试消息").getBytes());
            //同步消息发送
            SendResult sendResult = producer.send(msg);
            System.out.println("消息发送完毕： sendResult="+sendResult.toString());

//            TimeUnit.SECONDS.sleep(1);
        }

        //批量发送
//        List<Message> list = new ArrayList<>();
//        list.add(new Message(RocketmqConstant.topic1, "第1条测试消息".getBytes()));
//        list.add(new Message(RocketmqConstant.topic1, "第2条测试消息".getBytes()));
//        list.add(new Message(RocketmqConstant.topic1, "第3条测试消息".getBytes()));
//        SendResult sendResult = producer.send(list);

        producer.shutdown();

    }

}
