package com.guojing.jl.actuator;

import com.guojing.jl.actuator.service.TestService;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


/**
 * 、可通过 http://localhost:9091/actuator  查看所有暴漏的端点
 *
 *
 */
@SpringBootApplication
@EnableAdminServer
public class ActuatorSpringBootApplication {

    @Autowired

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ActuatorSpringBootApplication.class);

        TestService bean = context.getBean(TestService.class);
        bean.test();


    }


}
