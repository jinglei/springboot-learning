package com.guojing.jl.actuator.extend;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;

/**
 * 自定义端点
 * http://localhost:9091/actuator/myEndPoint
 */
@Component
@Endpoint (id = "myEndPoint")
public class MyEndPoint {

    @ReadOperation
    public Map show(){

        return Collections.singletonMap("enpoint1", "测试");
    }


    @WriteOperation
    public Map write(String key,String value){
        return Collections.singletonMap(key, value);
    }
}
