package com.guojing.jl.actuator.extend;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

/**
 * http://localhost:9091/actuator/health
 *
 * 自定义端点的指标
 * {
 *     "status": "UP",
 *     "components": {
 *         "db": {
 *             "status": "UP",
 *             "details": {
 *                 "database": "MySQL",
 *                 "validationQuery": "isValid()"
 *             }
 *         },
 *         "my": {
 *             "status": "UP",
 *             "details": {
 *                 "total": "test"
 *             }
 *         }
 *     }
 * }
 */
@Component
public class MyHealthIndicator extends AbstractHealthIndicator {


    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {

        boolean normal = check();
        if(normal){
//            builder.up();
            builder.up().withDetail("total","test");
        }else {
            builder.down();
        }
    }

    private boolean check() {

        return true;
    }


}
