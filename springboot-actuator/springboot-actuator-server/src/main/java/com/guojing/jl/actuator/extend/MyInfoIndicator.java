package com.guojing.jl.actuator.extend;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

/**
 * 自定义端点的指标
 * http://localhost:9091/actuator/info
 *
 * {
 *     "myInfo": "哈哈"
 * }
 */
@Component
public class MyInfoIndicator implements InfoContributor {


    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("myInfo","哈哈");
    }
}
