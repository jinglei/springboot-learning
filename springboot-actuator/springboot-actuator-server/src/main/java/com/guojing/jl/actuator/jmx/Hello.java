package com.guojing.jl.actuator.jmx;

/**
 * 该类名称必须与实现的接口的前缀保持一致（即MBean的前面的名称）
 * 要监控的类和 MBean 接口必需在同一包下
 */
public class Hello implements HelloMBean {

    private String name;

    private int age;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getAge() {
        return this.age;
    }

    @Override
    public String test(String p) {
        System.out.println(p);
        return "test";
    }

}
