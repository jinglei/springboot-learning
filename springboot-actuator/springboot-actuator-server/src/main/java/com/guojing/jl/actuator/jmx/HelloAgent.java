package com.guojing.jl.actuator.jmx;

import lombok.SneakyThrows;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;


/**
 * 使用Jconsole可以查看到注册的MBean，并且可以调用其中的方法，起到监控调试作用
 */
public class HelloAgent {

    @SneakyThrows
    public static void main(String[] args) {

        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();

        //ObjectName中的取名是有规范的：格式： "域名:name=Mbean名称"，其中域名和MBean的
        ObjectName objectName = new ObjectName("jmxBean:name=hello");

        platformMBeanServer.registerMBean(new Hello(), objectName);

        Thread.sleep(500000);

    }

}
