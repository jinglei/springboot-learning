package com.guojing.jl.actuator.jmx;

public interface HelloMBean {

    String getName();

    int getAge();

    String test(String p);
}
