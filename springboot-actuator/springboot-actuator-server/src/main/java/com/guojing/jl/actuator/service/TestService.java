package com.guojing.jl.actuator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class TestService {

    @Autowired
    private WebClient.Builder webClient;

    public void test(){
        System.out.println(webClient);
    }

}
