package com.guojing.jl.boot.admin.server.domain.entities;

import com.guojing.jl.boot.admin.server.domain.values.InstanceId1;
import lombok.Data;

@Data
@lombok.EqualsAndHashCode(exclude = { "unsavedEvents", "statusTimestamp" })
@lombok.ToString(exclude = "unsavedEvents")
public class Instance {

    private final long version;

    private InstanceId1 instanceId;
}
