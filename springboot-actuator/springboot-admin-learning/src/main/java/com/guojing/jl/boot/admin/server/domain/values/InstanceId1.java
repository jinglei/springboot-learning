package com.guojing.jl.boot.admin.server.domain.values;

import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * Value type for the instance identifier
 * @author jl
 */
@lombok.Data
public final class InstanceId1 implements Serializable, Comparable<InstanceId1> {

    private final String value;

    private InstanceId1(String value) {
        Assert.hasText(value, "'value' must have text");
        this.value = value;
    }

    public static InstanceId1 of(String value) {
        return new InstanceId1(value);
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public int compareTo(InstanceId1 that) {
        return this.value.compareTo(that.value);
    }

}
