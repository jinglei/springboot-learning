package com.guojing.jl.boot.admin.server.web.client;

@FunctionalInterface
public interface InstanceWebClientCustomizer {

    void customize(InstanceWebClient instanceWebClient);

}
