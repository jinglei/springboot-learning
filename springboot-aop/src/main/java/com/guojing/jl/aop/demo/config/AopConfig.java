package com.guojing.jl.aop.demo.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class AopConfig {
    //定义一个切点
    @Pointcut("execution(* com.guojing.jl.aop.demo.service.StudentService.studentAdd(..))")
    public void pointcut(){}

    //前置通知
    @Before("pointcut()")
    public void before(JoinPoint joinPoint){
        System.out.println("前置通知。。。");
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

    }

}
