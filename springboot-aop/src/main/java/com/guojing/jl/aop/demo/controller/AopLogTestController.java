package com.guojing.jl.aop.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: guojing
 * @create: 2022/12/14 12:02
 */
@RestController
@RequestMapping("/test")
public class AopLogTestController {
}
