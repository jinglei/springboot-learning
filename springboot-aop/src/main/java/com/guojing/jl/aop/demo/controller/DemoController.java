package com.guojing.jl.aop.demo.controller;

import com.guojing.jl.aop.demo.service.StudentService;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @Autowired
    private StudentService studentService;

    @GetMapping("demo")
    public int demo(){
        int i =  studentService.studentAdd(1,1);
        return i;
    }


    @GetMapping("non")
    public int nonAOP(){
        studentService.nonAOP();
        return 1;
    }

    @GetMapping("jdbcTemplate")
    public int jdbcTemplateTest(){

        studentService.testTranxaction();

        return 1;

    }
}
