package com.guojing.jl.aop.demo.controller;

import com.guojing.jl.aop.demo.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 事务测试
 */
@Controller("/tx")
public class TransactionController {

    @Autowired
    private IStudentService studentService;

    @GetMapping("/tese1")
    public String test1(){

        System.out.println("controller-----test1");
        studentService.testTransaction();
        System.out.println("controller--------end");

        return null;
    }

}
