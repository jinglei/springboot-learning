package com.guojing.jl.aop.demo.log.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@TableName("student")
public class Student {

    /**
     *
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Integer id;
    /**
     *
     */
    private String name;
    /**
     *
     */
    private int age;

}
