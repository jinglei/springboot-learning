package com.guojing.jl.aop.demo.log.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: guojing
 * @create: 2022/12/14 11:49
 */
@Data
public class SysLog  implements Serializable {
    private static final long serialVersionUID = -6309732882044872298L;
    private Integer id;
    private String userName;
    private String operation;
    private Integer time;
    private String method;
    private String params;
    private String ip;
    private Date createTime;
}
