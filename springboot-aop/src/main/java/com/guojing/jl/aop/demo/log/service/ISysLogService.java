package com.guojing.jl.aop.demo.log.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.guojing.jl.aop.demo.log.entity.SysLog;

/**
 * @author: guojing
 * @create: 2022/12/14 11:51
 */
public interface ISysLogService extends IService<SysLog> {
}
