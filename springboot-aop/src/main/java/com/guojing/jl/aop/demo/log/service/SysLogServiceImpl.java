package com.guojing.jl.aop.demo.log.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guojing.jl.aop.demo.log.entity.SysLog;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

/**
 * @author: guojing
 * @create: 2022/12/14 11:52
 */
@Service
public class SysLogServiceImpl implements ISysLogService {
    @Override
    public boolean saveBatch(Collection<SysLog> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean saveOrUpdateBatch(Collection<SysLog> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean updateBatchById(Collection<SysLog> entityList, int batchSize) {
        return false;
    }

    @Override
    public boolean saveOrUpdate(SysLog entity) {
        return false;
    }

    @Override
    public SysLog getOne(Wrapper<SysLog> queryWrapper, boolean throwEx) {
        return null;
    }

    @Override
    public Map<String, Object> getMap(Wrapper<SysLog> queryWrapper) {
        return null;
    }

    @Override
    public <V> V getObj(Wrapper<SysLog> queryWrapper, Function<? super Object, V> mapper) {
        return null;
    }

    @Override
    public BaseMapper<SysLog> getBaseMapper() {
        return null;
    }

    @Override
    public Class<SysLog> getEntityClass() {
        return null;
    }
}
