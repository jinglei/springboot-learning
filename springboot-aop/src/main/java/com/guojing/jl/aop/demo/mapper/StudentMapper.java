package com.guojing.jl.aop.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guojing.jl.aop.demo.log.entity.Student;

public interface StudentMapper extends BaseMapper<Student> {
}
