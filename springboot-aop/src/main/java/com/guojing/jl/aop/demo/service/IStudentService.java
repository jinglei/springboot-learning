package com.guojing.jl.aop.demo.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.guojing.jl.aop.demo.log.entity.Student;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

/**
 * @author: guojing
 * @create: 2022/12/13 22:39
 */
public interface IStudentService extends IService<Student> {

    public String testTransaction();
}
