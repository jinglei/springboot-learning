package com.guojing.jl.aop.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudentService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //这个方法被aop拦截
    public int studentAdd(int x,int y){

        System.out.println("studentAdd..." );

        return x+y;
    }

    //这个方法不被aop拦截
    public void nonAOP(){
        System.out.println("nonAOP.....");

    }

    @Transactional(rollbackFor = Exception.class)
    public void testTranxaction(){
        jdbcTemplate.execute("insert into test(name, age) value ('11', 22)");
        throw new NullPointerException();
    }
}
