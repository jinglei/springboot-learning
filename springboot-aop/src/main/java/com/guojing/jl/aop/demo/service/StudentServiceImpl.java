package com.guojing.jl.aop.demo.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.guojing.jl.aop.demo.log.entity.Student;
import com.guojing.jl.aop.demo.mapper.StudentMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.ws.ServiceMode;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {


    @Transactional(rollbackFor = Exception.class)
    @Override
    public String testTransaction() {
        System.out.println("111");
        return null;
    }
}
