package com.guojing.jl.springbootbase.annotation;

import com.guojing.jl.springbootbase.annotation.bean.Person;
import com.guojing.jl.springbootbase.annotation.config.TestConfigurationProxyBeanMethodsFalse;
import com.guojing.jl.springbootbase.annotation.service.User2Service;
import com.guojing.jl.springbootbase.annotation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringbootBaseApplication  {

    @Autowired
    private Person person;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SpringbootBaseApplication.class);

        TestConfigurationProxyBeanMethodsFalse methodsFalse = context.getBean(TestConfigurationProxyBeanMethodsFalse.class);
        Person person1 = methodsFalse.myPerson();
        Person person2 = methodsFalse.myPerson();
        System.out.println(person1);
        System.out.println(person2);


        UserService userService = context.getBean(UserService.class);
        User2Service userService2 = context.getBean(User2Service.class);
        System.out.println(userService.getPerson());
        System.out.println(userService2.getPerson());

    }
}
