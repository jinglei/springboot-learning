package com.guojing.jl.springbootbase.annotation.bean;

import javax.annotation.PostConstruct;

public class MyBean {

    public MyBean(){
        System.out.println("MyBean construct......");
    }
    @PostConstruct
    public void init(){
        System.out.println("MyBean PostConstruct ....");
    }
    public void sayHello(){
        System.out.println("Hi MyBean ,hello world!");
    }

}
