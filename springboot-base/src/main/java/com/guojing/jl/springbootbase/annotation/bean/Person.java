package com.guojing.jl.springbootbase.annotation.bean;

import lombok.Data;

public class Person {

    public String name;

    public Person(){
        System.out.println();
    }

    public Person(String name){
        this.name = name;
    }


}
