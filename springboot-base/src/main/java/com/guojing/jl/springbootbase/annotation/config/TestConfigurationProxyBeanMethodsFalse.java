package com.guojing.jl.springbootbase.annotation.config;


import com.guojing.jl.springbootbase.annotation.bean.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class TestConfigurationProxyBeanMethodsFalse {

    public TestConfigurationProxyBeanMethodsFalse(){
        System.out.println(1);
    }

    @Bean
    public Person myPerson(){
        return new Person("李逍遥");
    }
}
