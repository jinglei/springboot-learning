package com.guojing.jl.springbootbase.annotation.lambda;

import io.reactivex.CompletableOnSubscribe;

import java.util.function.*;

/**
 * @author: guojing
 * @create: 2023/6/8 23:26
 */
public class ConsumerFunction {

    public static void main(String[] args) {

        Consumer<String> stringConsumer = s -> System.out.println(s);

        Consumer<String> stringConsumer2 = System.out::println;
        stringConsumer.accept("hhha");
        stringConsumer2.accept("rrr");

        //静态方法使用类名::静态方法名
        Consumer<Dog> dogConsumer = dog -> Dog.say(dog);
        Consumer<Dog> dogConsumer2 = Dog::say;
        Dog dog = new Dog();
        dogConsumer2.accept(dog);

        //实例方法
        Function<String,String> dogConsumer4 = (name) -> dog.hello(name);
        Function<String,String> dogConsumer5 = dog::hello;
        String test = dogConsumer4.apply("test");
        String test2 = dogConsumer5.apply("test2");
        System.out.println(test);
        System.out.println(test2);

        //当参数和返回值一样时
        UnaryOperator<String> unaryOperator = dog::hello;

        //使用类名引用实例方法: 成员方法默认第一个参数是this，所以第一个参数是Dog对象
        BiFunction<Dog,String,String> biFunction = Dog::hello;

        //无参构造函数的方法引用: 没有输入只有输出
        Supplier<Dog> dogSupplier = Dog::new;
        //创建新的对象
        System.out.println(dogSupplier.get());

        //有参数构造函数的方法引用: 编译器自动寻找入参是String的构造函数
        Function<String,Dog> dogFunction = Dog::new;
        //创建新的对象
        System.out.println(dogSupplier.get());


        String s = "11";
        s = "2";
    }

    private static class Dog{

        private String name = "哮天犬";

        public Dog(){

        }

        public Dog(String name){
            this.name = name;
        }

        public Dog(String name, Integer age){
            this.name = name;
        }

        public static void say(Dog dog){
            System.out.println(dog.name);
        }

        public String hello(String name){
            System.out.println("实例方法:" + name);
            return name + ":" + 10;
        }

    }

}
