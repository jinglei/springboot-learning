package com.guojing.jl.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * 实体类关联了es, 如果指定的索引不存在会自动创建索引
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
//指定es中的文档
@Document(indexName = "product", shards = 3, replicas = 1)
public class Product {

    /**
     * 商品唯一标识
     */
    @Id
    private Long id;

    /**
     * 商品名称: 分词
     */
    @Field(type = FieldType.Text)
    private String title;

    /**
     * 分类名称: 不分词
     */
    @Field(type = FieldType.Keyword)
    private String category;

    @Field(type = FieldType.Double)
    private Double price;//商品价格

    @Field(type = FieldType.Keyword, index = false)
    private String images;//图片地址

    private Date createStartTime;

    private Date createEndTime;
}
