package com.guojing.jl.dto;

import com.guojing.jl.bean.Product;
import lombok.Data;

import java.util.List;

@Data
public class PageResult {

    private long total;

    private int pageSize;

    private int pageNo;

    private List<Product> rows;
    private int pages;

}
