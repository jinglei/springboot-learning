package com.guojing.hbase.learn.controller;

import com.guojing.hbase.learn.pojo.UserDeviceTable;
import com.guojing.hbase.learn.service.IHBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: jinglei
 */
@RestController
@RequestMapping("/hbase")
public class HbaseDemoController {

    @Autowired
    IHBaseService ihBaseService;

    @RequestMapping(value = "create",method = RequestMethod.GET)
    public String create(){
        UserDeviceTable table = new UserDeviceTable();
        ihBaseService.createTable(table);
        return "success";
    }
}
