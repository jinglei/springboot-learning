package com.guojing.hbase.learn.model;

import java.util.ArrayList;
import java.util.List;

public abstract class HBaseTable {
    private String tableName;
    protected List<String> columnFamily = new ArrayList<>();

    public HBaseTable(String tableName){
        this.tableName = tableName;
        setColumnFamily();
    }

    /**
     *
     * @return 获取表名
     */
    public String getTableName(){
        return this.tableName;
    }

    /**
     *
     * @return 获取表所有列簇List
     */
    public List<String> getColumnFamily(){
        return this.columnFamily;
    }
    protected abstract void setColumnFamily();

    /**
     *
     * @return 获取默认的列簇名（即列簇List第一个元素）
     */
    public String getDefaultColumnName(){
        return this.columnFamily.get(0);
    }
}

