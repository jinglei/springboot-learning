package com.guojing.hbase.learn.pojo;

import com.guojing.hbase.learn.model.HBaseTable;

import java.util.Collections;

public class UserDeviceTable extends HBaseTable {

    public UserDeviceTable() {
        super("user_device_id");
    }

    @Override
    protected void setColumnFamily() {
        this.columnFamily = Collections.singletonList("c");
    }
}
