package com.guojing.hbase.learn.regionhelper;

public interface RowKeyGenerator {
    byte [] nextId();
}
