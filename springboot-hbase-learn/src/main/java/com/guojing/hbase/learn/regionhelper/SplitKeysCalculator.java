package com.guojing.hbase.learn.regionhelper;

public interface SplitKeysCalculator {
    public byte[][] calcSplitKeys();
}
