/*
 * @(#)DirectInsertSort.java
 */
package com.jl.algorithm;

/**
 * <p>
 * 直接插入排序:对于基本有序的数组最好用，稳定的
 *
 *  在要排序的一组数中，假设前面(n-1)[n>=2] 个数已经是排
	好顺序的，现在要把第n个数插到前面的有序数中，使得这n个数
	也是排好顺序的。如此反复循环，直到全部排好顺序。
 * </p>
 *
 * @author: jl
 * @date: 2018/08/02
 * @version: v1.0
 */
public class DirectInsertSort {

	public static void main(String[] args) {
		int[] a = { 49, 38, 65, 97, 76, 13, 27, 49, 78, 34, 12, 64, 5, 4, 62, 99, 98, 54, 56, 17, 18, 23, 34, 15, 35,
				25, 53, 51 };
		insertSort(a);

		for (int i = 0; i < a.length; i++)
			System.out.print(a[i] +"  ");

	}

	public static void insertSort(int[] a) {
		for (int i = 1; i < a.length; i++) {
			for (int j = i ; j > 0; j--) {
				if(a[j] < a[j-1]){
					int temp = a[j];
					a[j] = a[j-1];
					a[j-1] = temp;
				}
			}
		}
	}


}
