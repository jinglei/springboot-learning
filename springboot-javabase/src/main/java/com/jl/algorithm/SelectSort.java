package com.jl.algorithm;

import java.util.Arrays;

/**
 * 从一组数据中，从0开始找出最小的放最前面，下一次从1的位置开始找出后面最小的放1位置，以此类推
 *
 * @description: 选择排序：最无用的排序,时间复杂度是O(n²)
 * @author: jl
 * @create: 2021/3/21 18:47
 */
public class SelectSort {


    public static void main(String[] args) {
        int[] arr = {5,6,2,8,1,9,3};
        for (int i = 0; i < arr.length-1; i++){
            int minPos = i;
            for (int j = i+1; j <arr.length ; j++) {
                if(arr[j] < arr[minPos]){
                    minPos = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[minPos];
            arr[minPos] = temp;
        }

        System.out.println(Arrays.toString(arr));
    }


}
