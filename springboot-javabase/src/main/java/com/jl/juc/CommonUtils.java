package com.jl.juc;

import java.util.StringJoiner;
import java.util.concurrent.TimeUnit;

public class CommonUtils {

    public static void sleepSecond(long second){
        try {
            TimeUnit.SECONDS.sleep(second);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void printThread(String msg){
        StringJoiner add = new StringJoiner("|")
                .add(Thread.currentThread().getName())
                .add(String.valueOf(Thread.currentThread().getId()))
                .add(msg);

        System.out.println(add.toString());
    }

}
