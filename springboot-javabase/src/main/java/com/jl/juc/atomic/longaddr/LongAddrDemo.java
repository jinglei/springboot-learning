package com.jl.juc.atomic.longaddr;

import org.junit.Test;

import java.util.concurrent.atomic.LongAccumulator;
import java.util.concurrent.atomic.LongAdder;

/**
 * jdk1.8下 LongAddr比AtomicInteger性能更好
 *
 *  当高并发情况下收集统计信息，但不用于精确的统计， LongAdder性能优于AtomicInteger
 *
 *  LongAddr.sum()方法在没有并发更新的情况下返回的是准确的值，存在并发的情况下,sum不保证返回精确值，也就是说sum统计并不是加锁（原子）统计
 *
 * 1、热点商品点赞计数器，点赞数加加统计，不要求实时准确
 *
 * 2、一个List里面都是int类型，如何实现累加。
 *
 *
 */
public class LongAddrDemo {

    public static void main(String[] args) {

        //只能从0开始，累加
        LongAdder longAdder = new LongAdder();
        longAdder.increment();
        longAdder.increment();
        longAdder.increment();
        System.out.println(longAdder.sum());
        System.out.println(longAdder.intValue());
    }

    @Test
    public void testLongAddrAccumulator(){

        LongAccumulator longAccumulator = new LongAccumulator((left, right) -> {
            return left + right ;
        }, 5);
        longAccumulator.accumulate(2);
        System.out.println(longAccumulator.get());

        longAccumulator.accumulate(3);
        System.out.println(longAccumulator.get());

    }

}
