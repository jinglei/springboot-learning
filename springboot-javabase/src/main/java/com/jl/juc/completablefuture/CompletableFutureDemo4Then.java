package com.jl.juc.completablefuture;

import com.jl.juc.CommonUtils;

import java.util.concurrent.CompletableFuture;

/**
 * then方法
 */
public class CompletableFutureDemo4Then {

    public static void main(String[] args) {
        CompletableFuture<Void> run = CompletableFuture.supplyAsync(() -> {
            CommonUtils.printThread("111");
            return "哈哈";
        }).thenApply((s) -> {
            //使用supplyAsync的线程
            CommonUtils.printThread("222");
            return s + "你好";
        }).thenAccept((s)->{
            CommonUtils.printThread(s);
        }).thenRun(() -> {
            //没有参数，并且使用新的线程
            CommonUtils.printThread("333");
        });

        Void join = run.join();
        System.out.println(join);


    }

}
