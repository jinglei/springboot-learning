package com.jl.juc.completablefuture;

import com.jl.juc.CommonUtils;

import java.util.concurrent.CompletableFuture;

/**
 * 编排两个有依赖关系的异步任务： A执行完彩才会执行B
 *
 * thenCompose 方法： 后一个CompletableFuture依赖前一个CompletableFuture返回的结果，
 */
public class CompletableFutureDemo4ThenCompose {

    public static void main(String[] args) {
        CompletableFuture<String> compose = CompletableFuture.supplyAsync(() -> {
            CommonUtils.printThread("111");
            CommonUtils.sleepSecond(1);
            return "A";
        }).thenCompose(s -> {
            //返回的是CompletableFuture
            return CompletableFuture.supplyAsync(() -> {
                CommonUtils.printThread("22");
                CommonUtils.sleepSecond(2);
                return s + "B";
            });
        });

        String join = compose.join();
        CommonUtils.printThread(join);


    }

}
