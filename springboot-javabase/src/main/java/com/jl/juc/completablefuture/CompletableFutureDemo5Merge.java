package com.jl.juc.completablefuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 *
 * 编排两个没有依赖关系的异步任务
 *
 * 对计算结果进行合并， 线程1和线程2都执行完之后再做某个事
 *
 *
 */
public class CompletableFutureDemo5Merge {

    public static void main(String[] args) {

        CompletableFuture<String> supplyAsyncA = CompletableFuture.supplyAsync(() -> {

            System.out.println(Thread.currentThread().getName() + " 来了1111");
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " 结束");
            return "A";
        });


        CompletableFuture<String> supplyAsyncB = CompletableFuture.supplyAsync(() -> {

            System.out.println(Thread.currentThread().getName() + " 来了啊222");
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " 结束");
            return "B";
        });

        CompletableFuture<String> completableFuture = supplyAsyncA.thenCombine(supplyAsyncB, (x, y) -> {
            System.out.println("A和B都结束了,A=" + x + ",B=" + y);
            return x + y;
        });
        System.out.println(completableFuture.join());


    }

}
