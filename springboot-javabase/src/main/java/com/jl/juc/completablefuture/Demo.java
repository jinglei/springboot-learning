package com.jl.juc.completablefuture;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Demo {

    public static void main(String[] args) {

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        CompletableFuture<String> stringCompletableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("1111111");
            return "hhh";
        }, executorService);

        /*CompletableFuture<Void> voidCompletableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("1111111");
            return "hhh";
        }).thenAccept(i -> {
            System.out.println(i);
        });
*/
//        System.out.println(voidCompletableFuture.join());


        /*CompletableFuture<String> apply = CompletableFuture.supplyAsync(() -> {
            System.out.println("1111111");
            return "hhh";
        }).thenApply(i -> {
            System.out.println(i);
            return "result:" + i;
        });
        System.out.println(apply.join());*/

    }
}
