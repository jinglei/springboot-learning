package com.jl.juc.locksupport;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * LockSupport.park 和 unpark 等待唤醒线程
 *
 *  与synchronize的 wait、notify 和  Lock的 await、singal 相比 支持先唤醒再等待
 *
 *   注意，
 *   （1）如果先执行了LockSupport的unpark再执行park 会导致后park的时候不会等待，没有阻塞效果
 *   （2）LockSupport.unpark会使得当前线程得到一个许可证，及时多次调用unpark，最多只会拿到一个通行证，
 *
 *
 */
public class LockSupportDemo {

    public static void main(String[] args) {


        Thread t1 = new Thread(() -> {

            try {

                System.out.println(Thread.currentThread().getName() + "来了");
                System.out.println(Thread.currentThread().getName() + "开始等待");

                //等待
                LockSupport.park();

                System.out.println(Thread.currentThread().getName() + "被唤醒");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "t1");
        t1.start();


        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try{
            System.out.println(Thread.currentThread().getName() + "来了啊啊啊啊");
            LockSupport.unpark(t1);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
