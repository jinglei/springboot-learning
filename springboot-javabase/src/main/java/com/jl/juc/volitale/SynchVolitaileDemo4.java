package com.jl.juc.volitale;

import org.springframework.boot.autoconfigure.security.SecurityProperties;

import java.util.concurrent.TimeUnit;

public class SynchVolitaileDemo4 {

    private static boolean flag = true;

    private static Object lock = new Object();

    private static SecurityProperties.User user = new SecurityProperties.User();

    /**
     *  测试对象的可见性
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {

        new Thread(()->{
            System.out.println(Thread.currentThread().getName()  + "线程-------- flag = " + flag);
                while (flag){
                    //此处加锁的目的是为了保证flag的可见性
                    System.out.println(Thread.currentThread().getName()  + "线程======= user.name =-" +  user.getName());
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            System.out.println(Thread.currentThread().getName()  + "线程----end------ flag = " + flag);
        }, "C").start();

        //保证线程C先启动
        TimeUnit.SECONDS.sleep(2);
        user.setName("11111");
//        flag = false;
        System.out.println(Thread.currentThread().getName() + "设置user.name = " + user.getName());
    }

}


