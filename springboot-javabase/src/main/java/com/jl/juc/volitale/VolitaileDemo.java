package com.jl.juc.volitale;

import java.util.concurrent.TimeUnit;

public class VolitaileDemo {

    private /*volatile*/ static boolean flag = true;

    /**
     * 可见性
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {

        new Thread(()->{
            System.out.println(Thread.currentThread().getName()  + "-------- falg = " + flag);
            while (flag){

            }
            System.out.println(Thread.currentThread().getName()  + "----end------ falg = " + flag);
        }, "A").start();

        //保证线程A先启动
        TimeUnit.SECONDS.sleep(2);

        flag = false;
        System.out.println(Thread.currentThread().getName() + "设置flag = " + flag);

    }

}


