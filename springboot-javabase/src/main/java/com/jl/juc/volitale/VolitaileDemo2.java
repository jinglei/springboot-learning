package com.jl.juc.volitale;

import java.util.concurrent.TimeUnit;

public class VolitaileDemo2 {

    private static boolean flag = true;
    private static Object lock = new Object();

    /**
     * 可见性
     * @param args
     */
    public static void main(String[] args) throws InterruptedException {

        new Thread(()->{
            System.out.println(Thread.currentThread().getName()  + "-------- falg = " + flag);
            synchronized (lock){
                while (flag){
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName()  + "----end------ falg = " + flag);
            }

        }, "A").start();

        //保证线程A先启动
        TimeUnit.SECONDS.sleep(2);

        synchronized (lock){
            flag = false;
            System.out.println(Thread.currentThread().getName() + "设置flag = " + flag);
            lock.notify();
        }


    }

}


