package com.jl.reflect;

import com.jl.reflect.Parent;

import java.lang.reflect.Field;

/**
 * @description: TODO
 * @author: jl
 * @create: 2021/9/10 22:33
 */
public class Children extends Parent {

    private String childName;

    private int childAge;


    public static void main(String[] args) throws Exception {
        Children c = new Children();
        Class<? extends Children> aClass = c.getClass();
        Field childName = aClass.getDeclaredField("childName");
        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field field : declaredFields) {
            String name = field.getName();
            System.out.println(name);
        }


    }

}
