package com.jl.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @description: TODO
 * @author: jl
 * @create: 2021/3/22 22:31
 */
public class FutureTaskDemo {

    public static void main(String[] args) {
        FutureTask<String> task = new FutureTask<>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("11111111");
                Thread.sleep(2000);
                return "futuretask call";
            }
        });

        new Thread(task).start();
        System.out.println("2222222");
        try {
            System.out.println(task.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
