package com.jl.thread.lock.reentrantLock;

/**
 * @description: sync中断
 * @author: jl
 * @create: 2021/3/19 9:30
 */
public class SyncInterruptedDemo {


    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            say();
        });
        Thread thread1 = new Thread(() -> {
            say();
        });
        thread.start();
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread1.start();
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread1.interrupt();
        System.out.println("thread1已经被中断");
    }

    public static void say(){
        System.out.println(Thread.currentThread().getName()+"等待获取锁");
        synchronized (SyncInterruptedDemo.class){
            System.out.println(Thread.currentThread().getName()+"成功获取锁");
            int  i= 0;
            while(i <= 2000000000L){
                i++;
            }

            System.out.println(Thread.currentThread().getName()+"释放获取锁");
        }

    }

}
