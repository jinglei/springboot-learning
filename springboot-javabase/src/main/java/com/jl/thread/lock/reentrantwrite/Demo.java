package com.jl.thread.lock.reentrantwrite;

import java.util.concurrent.*;

/**
 * @description: TODO
 * @author: jl
 * @create: 2020/6/3 22:12
 */
public class Demo {


    public static void main(String[] args) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 10, 60, TimeUnit.SECONDS, new SynchronousQueue());

        SynchronousQueue<String> synchronousQueue = new SynchronousQueue();



        System.out.println("11111111111");
        Future<Object> submit = threadPoolExecutor.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                System.out.println("读取线程");
                String take = synchronousQueue.take();
                return take;
            }

        });

        try {
            System.out.println("还没放入元素");
            synchronousQueue.put("1");
            System.out.println("放入了元素");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("2222222222");
        try {
            System.out.println("获取到元素"+submit.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

}
