package com.jl.thread.locksport;

import java.util.concurrent.locks.LockSupport;

/**
 * 线程交替打印：A1B2C3D4
 *
 * @description: TODO
 * @author: jl
 * @create: 2020/5/25 21:55
 */
public class LockSupportDemo {

    static Thread t1,t2=null;

    public static void main(String[] args) {
        char[] a1 = "ABCDEF".toCharArray();
        char[] a2 = "123456".toCharArray();

        t1 = new Thread(() ->{
            for (char c : a1){
                System.out.print(c);
//                System.out.println("unpark--------------111");
                LockSupport.unpark(t2);
//                System.out.println("unpark--------------222");
                LockSupport.park();
            }
        });

        t2 = new Thread(() ->{
            for (char c : a2){
                LockSupport.park();
                System.out.print(c);
                LockSupport.unpark(t1);
            }
        });
        t1.start();
        t2.start();

    }

}
