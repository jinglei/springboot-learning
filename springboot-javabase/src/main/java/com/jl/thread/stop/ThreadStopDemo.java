package com.jl.thread.stop;

import java.util.concurrent.TimeUnit;

/**
 * 线程停止实例
 */
public class ThreadStopDemo {

    public static void main(String[] args) {

        Thread t1 = new Thread(() -> {

            while (!Thread.currentThread().isInterrupted()) {
                System.out.println("哈哈");
            }

            System.out.println("终止了");

        });
        t1.start();

        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t1.interrupt();


    }

}
