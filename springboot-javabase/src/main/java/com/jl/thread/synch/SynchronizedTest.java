package com.jl.thread.synch;

import org.openjdk.jol.info.ClassLayout;

/**
 * @description: TODO
 * @author: jl
 * @create: 2021/3/14 18:20
 */
public class SynchronizedTest {

    public static void main(String[] args) {
        Object o = new Object();
        String s = ClassLayout.parseInstance(o).toPrintable();
        System.out.println(s);

        synchronized (o){
            System.out.println(ClassLayout.parseInstance(o).toPrintable());

        }

    }
}
