package com.jl.thread.threadpool;

import java.util.concurrent.*;

/**
 * @description: TODO
 * @author: jl
 * @create: 2021/3/22 22:23
 */
public class CallableDemo {



    public static void main(String[] args) {

        ExecutorService executorService = Executors.newCachedThreadPool();
        Future<String> future = executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(3000);
                System.out.println("333333333");
                return "test";
            }
        });

        System.out.println("11111111111");
        try {
            String s = future.get();
            System.out.println("22222222222");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }

}
