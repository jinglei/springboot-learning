package com.jl.thread.waitnotify;

/**
 * @description: TODO
 * @author: jl
 * @create: 2020/5/24 21:56
 */
public class Demo1 {

    private static volatile int i = 3;
    private static Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {

        new Thread(()->{
            synchronized (lock){
                System.out.println("111111111---------1");
                i = 5;
                try {
                    lock.wait();
                    System.out.println("111111111---------2");
                    System.out.println("i---------" + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("111111111---------3");

            }

        }).start();

        Thread.sleep(200);
        new Thread(()->{
            synchronized (lock){
                System.out.println(i);
                System.out.println(222222222);
                i = 9;
                lock.notify();
            }

        }).start();

    }
}
