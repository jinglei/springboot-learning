package com.jl.thread.waitnotify;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.LockSupport;

/**
 * @description: 线程交替输出A1B2C3
 * @author: jl
 * @create: 2020/5/25 22:11
 */
public class Demo2 {


    static Object lock = new Object();

    static Thread t1,t2=null;

    public Demo2() {
    }

    public static void main(String[] args) throws InterruptedException {
        char[] a1 = "ABCDEF".toCharArray();
        char[] a2 = "123456".toCharArray();
        CountDownLatch latch = new CountDownLatch(1);

        t1 = new Thread(() ->{
            synchronized (lock){
                latch.countDown();
                for (char c : a1){
                    System.out.print(c);
                    try {
                        lock.notify();
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                lock.notify();

            }



        });
        t1.start();
        latch.await();

        t2 = new Thread(() ->{
            synchronized (lock){
                for (char c : a2){
                    System.out.print(c);
                    try {
                        lock.notify();
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                lock.notify();
            }
        });
        t2.start();

    }




}
