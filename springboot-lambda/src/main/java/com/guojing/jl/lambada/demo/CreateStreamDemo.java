package com.guojing.jl.lambada.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 流的创建
 */
public class CreateStreamDemo {


    public static void main(String[] args) {

        //1. list
        List<String> list = new ArrayList<>();
        list.stream();
        list.parallelStream();

        //2.Arrays
        Arrays.stream(new int[]{1,2});

        //3.数字流
        IntStream.of(1,2,3);
        IntStream.range(10, 20);
        IntStream.rangeClosed(10, 20);

        //54Random
        Random random = new Random();
        random.ints().filter(i -> i > 1000 && i < 3000).limit(10).forEach(i -> System.out.println(i));

        //5.
        Stream.of(1,2,3);
        Stream.of(list);
        Stream.generate(()-> random.nextInt()).limit(15);


    }

}
