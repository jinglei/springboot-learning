package com.guojing.jl.lambada.demo;

public class Demo1 {

    @FunctionalInterface
    interface IUser {
        String getName(String s);

        default int add(int x, int y){
            return x + y;
        }
    }


    public static void main(String[] args) {

        IUser user1 = (s)-> s + "1";
        System.out.println(user1.getName("的"));

    }




}
