package com.guojing.jl.lambada.demo;

import java.util.function.Function;

public class Demo2 {

    static class MyPeople {

        int age ;

        public String say(Function<Integer,String> callback){
            String apply = callback.apply(age);
            return apply;
        }

        public MyPeople(int age){
            this.age = age;
        }

    }

    public static void main(String[] args) {

        MyPeople myPeople = new MyPeople(12);

        Function<Integer,String> callback1 = age1 ->  "我自定义函数返回age=" + age1;

        String say = myPeople.say(callback1.andThen((a) ->  a +"，再链式调用返回" ));
        System.out.println(say);


    }




}
