package com.guojing.jl.lambada.demo;

import com.guojing.jl.lambada.demo.bean.User;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ListDemo {

    public static void main(String[] args) {

        List<Integer> collect = IntStream.rangeClosed(1, 10).boxed().collect(Collectors.toList());
        System.out.println(collect);

        List<User> userList = IntStream.rangeClosed(1, 10)
                .mapToObj(i -> new User("User" + i, i))
                .collect(Collectors.toList());
        System.out.println(userList);


        //flatMap
        Map<String, List<User>> map = userList.stream().collect(Collectors.groupingBy(User::getName));
        Collection<List<User>> values = map.values();
        List<User> list = values.stream().flatMap(users -> users.stream()).collect(Collectors.toList());
        System.out.println(list.size());


    }

}
