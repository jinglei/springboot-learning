package com.guojing.jl.lambada.demo.bean;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
public class User {

    private String name;

    private int age;

    private List<String> children;

    public User(String name, int age){
        this.name = name;
        this.age = age;
    }


}
