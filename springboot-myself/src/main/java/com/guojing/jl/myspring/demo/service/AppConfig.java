package com.guojing.jl.myspring.demo.service;


import com.guojing.jl.myspring.demo.spring.annotation.CompentScan;

/**
 * @author jl
 */
@CompentScan("com.guojing.jl.myspring.demo")
public class AppConfig {

}
