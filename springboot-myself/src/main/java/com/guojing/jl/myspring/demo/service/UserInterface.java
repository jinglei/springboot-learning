package com.guojing.jl.myspring.demo.service;

/**
 * @description:
 * @author: guojing
 * @create: 2023/4/20 21:50
 */
public interface UserInterface {

    public String getName(String sex);

}
