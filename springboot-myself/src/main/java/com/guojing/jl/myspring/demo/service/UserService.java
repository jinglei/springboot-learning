package com.guojing.jl.myspring.demo.service;

import com.guojing.jl.myspring.demo.spring.BeanNameAware;
import com.guojing.jl.myspring.demo.spring.InitializingBean;
import com.guojing.jl.myspring.demo.spring.annotation.Autowired;
import com.guojing.jl.myspring.demo.spring.annotation.Compent;
import com.guojing.jl.myspring.demo.spring.annotation.Scope;

@Compent
@Scope(false)
public class UserService implements BeanNameAware, InitializingBean,UserInterface {

    @Autowired
    private OrderService orderService;

    private String beanName;

    public void test() {
        System.out.println(orderService);
    }

    @Override
    public void setName(String name) {
        beanName = name;
    }

    @Override
    public void afterPropertieSet() {

        System.out.println("userService------------afterPropertieSet");
    }

    @Override
    public String getName(String sex) {
        return "hallo";
    }
}

