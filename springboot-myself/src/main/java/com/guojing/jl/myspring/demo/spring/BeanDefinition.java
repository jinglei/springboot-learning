package com.guojing.jl.myspring.demo.spring;

import lombok.Data;

/**
 * @author: guojing
 * @create: 2023/4/18 22:05
 */
@Data
public class BeanDefinition {

    private Class type;

    private String beanName;

    //单例还是多例
    private boolean single = true;

}
