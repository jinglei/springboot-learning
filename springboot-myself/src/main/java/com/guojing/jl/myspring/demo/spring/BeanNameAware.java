package com.guojing.jl.myspring.demo.spring;/**

/**
 *@description: bean名称回调
 *@author: jl
 *@create: 2023/4/18 22:45
 */
public interface BeanNameAware {

    public void setName(String name);
}
