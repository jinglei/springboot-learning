package com.guojing.jl.myspring.demo.spring;

/**
 *@description:
 *@author: jl
 *@create: 2023/4/20 21:33
 */
public interface BeanPostProcessor {

    public Object postProcessBeforeInitialization(String beanName, Object bean);

    public Object postProcessAfterInitialization(String beanName, Object bean);

}
