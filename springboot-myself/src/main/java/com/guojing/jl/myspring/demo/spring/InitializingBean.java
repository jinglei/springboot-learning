package com.guojing.jl.myspring.demo.spring;
/**
 *@description: bean初始化接口
 *@author: jl
 *@create: 2023/4/18 22:51
 */
public interface InitializingBean {

    public void afterPropertieSet();

}
