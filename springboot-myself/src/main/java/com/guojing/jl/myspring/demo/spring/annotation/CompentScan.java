package com.guojing.jl.myspring.demo.spring.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义扫描注解
 */
@Target(ElementType.TYPE)

//SOURCE:      * 注解在编译期间会被丢弃，就是如果使用这个属性，这个注解只会保留在源码上，编译之后是没有的。
// class  这个是默认值，保留在编译之后的class上，运行的时候获取不到。
//runtime 保留在运行期间，在运行的时候可以通过工具类获取到这个注解，这个注解的属性没有什么实际意义。
@Retention(RetentionPolicy.RUNTIME)
public @interface CompentScan {


    /**
     * 扫描路径
     * @return
     */
    String value() default "";

}
