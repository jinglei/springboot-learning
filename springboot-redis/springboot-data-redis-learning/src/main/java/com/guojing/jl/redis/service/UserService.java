package com.guojing.jl.redis.service;

import com.guojing.jl.redis.pojo.User;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: guojing
 * @create: 2022/12/17 22:48
 */
@Service
public class UserService implements InitializingBean {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    public void testString() {
        // 写入一条String数据
        redisTemplate.opsForValue().set("name", "虎哥");
        // 获取string数据
        Object name = redisTemplate.opsForValue().get("name");
        System.out.println("name = " + name);
    }

    public void testSaveUser() {
        // 写入数据
        redisTemplate.opsForValue().set("user:100", new User("虎哥", 21));
        // 获取数据
        User o = (User) redisTemplate.opsForValue().get("user:100");
        System.out.println("o = " + o);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("1111111111");
    }
}
