package com.guojing.redis;

import com.guojing.redis.dto.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class RedisTemplateTests {

    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    @Test
    void testString() {
        // 写入一条String数据
        redisTemplate.opsForValue().set("name", "虎哥");
        // 获取string数据
        Object name = redisTemplate.opsForValue().get("name");
        System.out.println("name = " + name);
    }

    /**
     * 不推荐使用，推荐使用 com.guojing.redis.RedisStringTemplateTests#testSaveUser()
     *
     * 直接序列化对象,reids中存储为：
     * "{\"@class\":\"com.guojing.redis.dto.User\",\"name\":\"\xe8\x99\x8e\xe5\x93\xa5\",\"age\":21}"
     *
     * 多了class属性， 就是根据这个属性记性自动序列化的。
     * 当对象很多时容易造成内存浪费，因此不建议使用自动序列化对象的方式。建议使用key和value都是string，自己手动序列化
     *
     */
    @Test
    void testSaveUser() {
        // 写入数据
        redisTemplate.opsForValue().set("user:100", new User("虎哥", 21));
        // 获取数据
        User o = (User) redisTemplate.opsForValue().get("user:100");
        System.out.println("o = " + o);
    }
}
