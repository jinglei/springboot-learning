package com.guojing.jl.webclient.learning;


import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.function.Consumer;

public class A_WebClientCreateDemo {


    @Test
    public void test1(){

        Mono<String> mono = WebClient.create()
                .method(HttpMethod.GET)
                .uri("http://ctrlcv.club/")
                //获取响应结果
                .retrieve()
                //将结果转换为指定类型
                .bodyToMono(String.class);

        //block方法返回最终调用结果，block方法是阻塞的
        System.out.println("响应结果:" + mono.block());

    }


    /**
     * WebClient.create(String baseUrl)：指定baseUrl，使用该客户端发送请求都是基于baseUrl。
     */
    @Test
    public void test2(){

        Mono<String> mono = WebClient
                //创建WenClient实例，指定基础url，所以下面uri请求的路径都是基于这个路径
                .create("http://ctrlcv.club/")
                //方法调用，WebClient中提供了多种方法
                .method(HttpMethod.GET)
                //请求url
                .uri("/share/175")
                //获取响应结果
                .retrieve()
                //将结果转换为指定类型
                .bodyToMono(String.class);
        //block方法返回最终调用结果，block方法是阻塞的
        System.out.println("响应结果：" + mono.block());

    }


    /**
     * 使用Mono和Flux接收返回结果，一个Mono对象包含0个或1个元素，而一个Flux对象包含1个或多个元素。
     */
    @Test
    public void testFlux() {
        Flux<User> userFlux = WebClient
                .create()
                .method(HttpMethod.GET)
                .uri("http://localhost:8080/hello")
                .retrieve()
                .bodyToFlux(User.class);
        List<User> users = userFlux.collectList().block();
    }

    /**
     * subscribe()非阻塞式获取响应结果
     */
    @Test
    public void testSubscribe() {
        Mono<String> mono = WebClient
                .create()
                .method(HttpMethod.GET)
                .uri("http://localhost:8080/hello")
                .retrieve()
                .bodyToMono(String.class);

        mono.subscribe(A_WebClientCreateDemo::handleMonoResp);

        mono.subscribe(System.out::println);
        mono.subscribe(new Consumer<String>() {
            @Override
            public void accept(String s) {

            }
        });

        mono.subscribe(s -> System.out.println(s));



    }
    //响应回调
    private static void handleMonoResp(String monoResp) {
        System.out.println("请求结果为：" + monoResp);
    }


    /**
     * exchange()获取HTTP响应完整内容
     */
    @Test
    public void testExchange() {
        Mono<ClientResponse> clientResponseMono = WebClient
                .create()
                .method(HttpMethod.GET)
                .uri("http://localhost:8080/hello")
                .exchange();

        ClientResponse clientResponse = clientResponseMono.block();
        //响应头
        ClientResponse.Headers headers = clientResponse.headers();
        //响应状态
        HttpStatus httpStatus = clientResponse.statusCode();
        //响应状态码
        int rawStatusCode = clientResponse.rawStatusCode();
        //响应体
        Mono<String> mono = clientResponse.bodyToMono(String.class);
        String body = mono.block();
    }



}
