package com.guojing.jl.webclient.learning;


import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

/**
 * 一旦webclient被创建, 它就是不可变的, 不过可以通过创建另外一个实例来重新指定属性:
 */
public class B_WebClientBuilderDemo {


    @Test
    public void testParam1() {
        Mono<String> mono = WebClient
                .builder()
                //配置头信息，或者其他信息
                .defaultHeader("token", "123456789")
                //创建WebClient实例
                .build()
                //方法调用，WebClient中提供了多种方法
                .method(HttpMethod.GET)
                //请求url
                .uri("http://localhost:8080/hello")
                //获取响应结果
                .retrieve()
                //将结果转换为指定类型
                .bodyToMono(String.class);
    }


    /**
     * 参数名占位符
     */
    @Test
    public void testParam2() {
        String url = "http://localhost:8080/user/{id}/{name}";
        String id = "123";
        String name = "Boss";
        Mono<String> mono = WebClient.create()
                .method(HttpMethod.POST)
                .uri(url, id, name)
                .retrieve()
                .bodyToMono(String.class);
        String result = mono.block();
    }


    /**
     * map传参
     */
    @Test
    public void testParamMap() {
        String url = "http://localhost:8080/user/{id}/{name}";
        Map<String, String> params = new HashMap<>();
        params.put("id", "123");
        params.put("name", "Boss");
        Mono<String> mono = WebClient.create()
                .method(HttpMethod.POST)
                .uri(url, params)
                .retrieve()
                .bodyToMono(String.class);
        String result = mono.block();
    }


}
