package com.guojing.jl.webclient.learning;


import org.springframework.http.HttpMethod;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

public class WebClientParamDemo {

    public static void main(String[] args) throws Exception {

        //数字占位符
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        String url = "http://localhost:8080/user/{1}/{2}";
        Mono<String> mono = WebClient.create()
                .method(HttpMethod.POST)
                //参数
                .uri(url, list.toArray())
                .retrieve()
                .bodyToMono(String.class);
        String result = mono.block();
    }

}
